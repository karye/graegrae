---
description: Sätt på "användarens glasögon" och beskriv produkten
---

# 1. Specifikation

## **Kundönskemål**

* Vad önskar sig kunden av webbtjänsten?
* Vad har man webbtjänsten till?
* Fokusera på användarnas behov snarare än avsändarens behov (Vad besökarna vill hitta istället för vad du vill visa)
  * Utgångspunkt för användbarhet
  * Tex "vad ska ligga längst fram/vara tydligast?" – det de minst datorvana användarna är mest intresserade av, kanske

## **Identifiera målgrupp och typanvändare**

* Vilken är webbtjänsten **målgrupp**?
* Konkretisera målgruppen med en **typanvändare**
  * Skriv ner typanvändare (minst två)
  * Namn, ålder, kort beskrivning
  * Varför besöker hen webbsidan?
  * Datorvana?
  * Vana vid längre/kortare texter?
  * Vad de vill hitta på sidan
  * Annat relevant
  * Prova [https://randomuser.me](https://randomuser.me)

{% embed url="https://www.youtube.com/watch?v=CkENZDjG1Sk" %}
