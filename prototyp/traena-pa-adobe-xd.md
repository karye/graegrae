---
description: Adobe XD är ett bra verktyg för att skapa prototyper
---

# Träna på Adobe XD

## Getting Started in Adobe XD with Howard Pinsky

* Ladda ned 5 bilder från [Unsplash.com](https://unsplash.com)
* Följ instruktionerna och skapa prototypen

{% embed url="https://youtu.be/4H2FZfoXnrs" %}
Del 1:2
{% endembed %}

{% embed url="https://youtu.be/tJ75p8h8okM" %}
Del 2:2
{% endembed %}

## How to design a simple website in Adobe XD for beginners

* Ladda ned filen, följ instruktionerna och skapa prototypen

{% file src="../.gitbook/assets/XD Basics 01.zip" %}
XD Basics 01
{% endfile %}

{% embed url="https://youtu.be/jHBMjjLY0Dw" %}
