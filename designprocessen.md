# Designprocessen

## Projektstart

### Designprocessens 5 steg

* Designen skapas enligt följande 5 steg
* Arbetet dokumenteras i projektplanen

![](.gitbook/assets/Designprocessen.png)

### Projektplanen

![](<.gitbook/assets/image (1).png>)

* [Projektplanen ](https://docs.google.com/document/d/1QsTan50\_hnbHWWK3wsVrbHtxXLVXy8F227YhA1SK7Ks/edit)skrivs före all kodning med att bygga webbplatsen
* [Projektplanen ](https://docs.google.com/document/d/1QsTan50\_hnbHWWK3wsVrbHtxXLVXy8F227YhA1SK7Ks/edit)innehåller färdiga rubriker och exempeltext
* Svara på frågorna under rubrikerna

### Loggboken

![](.gitbook/assets/image.png)

* [Loggboken ](https://docs.google.com/document/d/1QprqvSD6KP19W50SmPnmPy7r8-rEcAcvxSp1\_Pvjd-E/edit#heading=h.nj23sjpj5u97)fylls efter varje arbetspass, efter varje lektion
* Svara på frågorna under rubrikerna
* Dokumentera gärna med skärmdumpar

## Projektslut

![](<.gitbook/assets/image (2).png>)

* [Dokumentationen ](https://docs.google.com/document/d/1z\_X6LrJWrRpVDt9D5HZRO428F2ETc5kzgNwrd1kf8nI/edit)skrivs efter kodningen, när webbplatsen byggts klart
* Svara på frågorna under rubrikerna
* Dokumentera gärna med skärmdumpar
