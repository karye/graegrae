# Introduktion

## Vad är gränssnittsdesign?

{% embed url="https://www.youtube.com/watch?v=2wZUTe70w1Y&feature=youtu.be" %}
Design vs användarupplevelse
{% endembed %}

## Förberedelser

* Installera Adobe XD
* Skapa en mapp Gränssnittsdesign på datorn och på Google Drive
* Glöm inte 5 min i slutet för att fylla i loggen
