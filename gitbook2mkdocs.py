#!/usr/bin/python3

import glob
import os
import shutil
import re
import urllib.parse

print("Starting...")

# Tabsize in spaces
tabsize = 2

# Root folder for mkdocs
doc_root = "docs/"

# All assets files
img_source = ".gitbook/assets/"
img_dest = "images/"

# Get all folders
folders = glob.glob("*/")

# Create docs/images
if not os.path.exists(doc_root + img_dest):
    os.makedirs(doc_root + img_dest)
    print("... created: " + doc_root + ", " + doc_root + img_dest)

    # Copy md-pages tree to docs/
    for md_file in glob.glob("*.md"):
        shutil.copy(md_file, "docs/" + md_file)

    # Copy all folders to docs/
    for folder in folders:
        shutil.copytree(folder, "docs/" + folder)
else:
    print("... please delete docs/")
    print("Aborting!")
    exit()

# Move into docs/
os.chdir(doc_root)

# replace gitbook hint and file extensions to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% hint style="warning" %}
# some text
# {% endhint %}
# --------- to ---------
# !!! warning
#     some text
gb_hint_rg = r'{% hint style=\"(.*)\" %}(.*|[\s\S]+?){% endhint %}'
def hint_group(groups):
    hint_type = groups.group(1)
    hint_content = groups.group(2)
    hint_content = hint_content.replace("\n", "\n\t")
    return "!!! " + hint_type + "\n" + hint_content

# replace gitbook tab and file extensions to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% tab title="file.txt" %}
# some text
# {% endtab %}
# --------- to ---------
# ### warning
#     some text
gb_tab_rg = r'{% tab title=\"(.*)\" %}(.*|[\s\S]+?){% endtab %}'
def tab_group(groups):
    tab_type = groups.group(1)
    tab_content = groups.group(2)
    tab_content = tab_content.replace("\n", "\n\t")
    return "=== \"" + tab_type + "\"\n" + tab_content

# replace gitbook embed to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% embed url="https://youtu.be/..." %}
gb_embedY_rg = r'{% embed url=\"https://w*\.*youtu.*/(.*)\" %}'
def embedY_group(groups):
    embedY_url = groups.group(1)
    return "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/" + embedY_url + "\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"
    
# replace gitbook embed to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% embed url="..." %}
gb_embed_rg = r'{% embed url=\"(.*)\" %}'
def embed_group(groups):
    embed_url = groups.group(1)
    #return "<div class=\"embed\">[" + embed_url  + "](" + embed_url + ")</div>"
    return "<div class=\"embed\"><i class=\"fas fa-link\"></i><a href=\"" + embed_url  + "\">" + embed_url + "</a></div>"

# replace gitbook embed to mkdocs-compatible format. e.g.
# --------- replace ---------
# {% file src="..." %}
assets_array = {}
gb_file1_rg = r'{% file src=\"(.*)\" %}\n(.*|[\s\S]+?)\n{% endfile %}'
def file1_group(groups):
    global assets_array

    file_src = groups.group(1)
    asset = groups.group(2)
    # Add asset to list if not found in array
    if asset not in assets_array:
        assets_array[asset] = asset

    return "!!! file\n\n\t[" + asset  + "](" + urllib.parse.quote(file_src) + ")"

gb_file2_rg = r'{% file src=\"(.*)\" %}'
def file2_group(groups):
    global assets_array

    file_src = groups.group(1)
    asset = file_src.split('/')[-1]
    # Add asset to list if not found in array
    if asset not in assets_array:
        assets_array[asset] = asset
        
    return "!!! file\n\n\t[" + asset + "](" + urllib.parse.quote(file_src) + ")"

# Find image
img_index = 1
gb_img_rg = r'\!\[.*\]\(<?(.*)/(.*)>?\)'
def img_group(groups):
    global assets_array
    global img_index

    img_path = groups.group(1)
    img_src = groups.group(2)

    # Fix ending '>'
    img_src = img_src.replace('>', '');

    img = os.path.basename(img_src)
    img_ext = os.path.splitext(os.path.basename(img_src))[1]

    # Add image to list if not found in array
    if img not in assets_array:
        assets_array[img] = "image-" + str(img_index) + img_ext
        img_index += 1

    print("... " + img_path + ", " + img_src + " >> " + assets_array[img])
    return "![](" + img_path + "/" + assets_array[img] + ")"

gb_figure_rg = r'<figure><img src=\"(.*)/(.*)\" alt=\"\"><figcaption></figcaption></figure>'
def figure_group(groups):
    global assets_array
    global img_index

    img_path = groups.group(1)
    img_src = groups.group(2)

    # Fix ending '>'
    img_src = img_src.replace('>', '');

    img = os.path.basename(img_src)
    img_ext = os.path.splitext(os.path.basename(img_src))[1]

    # Add image to list if not found in array
    if img not in assets_array:
        assets_array[img] = "image-" + str(img_index) + img_ext
        img_index += 1

    print("... " + img_path + ", " + img_src + " >> " + assets_array[img])
    return "![](" + img_path + "/" + assets_array[img] + ")"

# Recursiv replace in all *.md
print("\nStarting copying md-pages to " + doc_root + " ...")
for md_file in glob.glob("**/*.md", recursive=True):
    with open(md_file, 'r') as file:
        print("... parsing: " + md_file)
        filedata = file.read()

    # Replace path for assets
    filedata = filedata.replace(img_source, img_dest)

    filedata = re.sub(gb_hint_rg, hint_group, filedata)
    filedata = re.sub(gb_tab_rg, tab_group, filedata)
    filedata = re.sub(gb_embedY_rg, embedY_group, filedata)
    filedata = re.sub(gb_embed_rg, embed_group, filedata)

    # Long or short file description?
    filedataTuple = re.subn(gb_file1_rg, file1_group, filedata)
    if filedataTuple[1] == 0:
        filedata = re.sub(gb_file2_rg, file2_group, filedata)
    else:
        filedata = filedataTuple[0]
    
    filedata = filedata.replace("{% tabs %}\n", "")
    filedata = filedata.replace("{% endtabs %}\n", "")
    filedata = filedata.replace("{% endembed %}\n", "")

    # https://www.markdownguide.org/basic-syntax/#line-break-best-practices
    filedata = filedata.replace("\\\n", "  \n")

    # Replace tags
    filedata = filedata.replace("\<", "&lt;")
    filedata = filedata.replace("\>", "&gt;")

    # Find images
    filedata = re.sub(gb_img_rg, img_group, filedata)
    filedata = re.sub(gb_figure_rg, figure_group, filedata)

    with open(md_file, 'w') as file:
        file.write(filedata)

print("... done copying md-pages tree")

# Replace nav with SUMMARY.md
print("\nStarting rebuilding nav ...")
subpage_match_rg = r'## (.*)\n(.*|[\s\S]+?)(\Z|(?=##))'
def subpage_group(groups):
    title = groups.group(1)
    title = re.sub(" <a.*>", "", title)
    content = groups.group(2)
    content = content.replace("- '", "  - '")
    return "  - '" + title + "':\n" + content

with open('SUMMARY.md', 'r') as file:
    nav_content = file.read()

nav_content = nav_content.replace('# Table of contents', 'nav:')
nav_content = nav_content.replace('\n\n', '\n')
nav_content = re.sub(r"\* \[(.*)\]\((.*)\)", r"  - '\1': \2", nav_content)
nav_content = re.sub(subpage_match_rg, subpage_group, nav_content)

# Rebuild nav YAML-compliant
indent = 0
indent_pre = 0
pre = ""
lines = nav_content.splitlines()
for index, line in enumerate(lines):
    # Find new subgroup
    # Count indent length
    indents = re.findall(r"^( +)- '", line)
    if len(indents) > 0:
        indent = len(indents[0])
    else:
        indent = 0
    if (indent > tabsize and indent > indent_pre):
        # Check if there is a page
        if len(lines[index - 1].split("': ")) == 2:
            # Insert page as subpage
            lines.insert(index, "  " + lines[index - 1])
            # Remove page from subgroup
            lines[index - 1] = lines[index - 1].split("': ", 1)[0] + "':"
    indent_pre = indent
    pre = line
nav_content = "\n".join(lines)

print("... collected nav from SUMMARY.md")
print(nav_content)

# Write nav changes to yml
os.chdir('..')

# Remove old nav
print("... opening nav in mkdocs.yml")
num = 0
lines = 0
found_nav = False
with open('mkdocs.yml', 'r') as file:
    lines = file.readlines()
    for num, line in enumerate(lines):
        if "nav:" ==  line[0:4]:
            found_nav = True
            print('... found nav: at line:', num)
            break
if num != 0:
    with open('mkdocs.yml', 'w') as file:
        file.writelines(lines[:num - 1])

# Add new nav?
if found_nav:
    with open('mkdocs.yml', 'a') as file:
        file.write("\n" + nav_content)
    print("... added nav in mkdocs.yml")
else:
    print("... could not replace nav in mkdocs.yml")

# Copy and rename images used i md-pages
print("\nStarting renaming images ...")
if os.path.exists(img_source):
    for key, value in assets_array.items():
        #print(key + " >> " + value)
        if os.path.exists(img_source + key):
            shutil.copy(img_source + key, doc_root + img_dest + value)
        else:
            print("... missing: " + img_source + key)
    print("... all images renamed and copied")
else:
    print("... could not copy images to docs/")

print("Done!")