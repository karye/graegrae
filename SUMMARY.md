# Table of contents

* [Introduktion](README.md)
* [Vad är gränssnittsdesign?](vad-aer-graenssnittsdesign.md)
* [Designprocessen](designprocessen.md)
* [1. Specifikation](specifikation.md)
* [2. Moodboard](moodboard.md)
* [3. Stilguide](stilguide.md)
* [4. Wireframe](wireframe.md)
* [5. Prototyp](prototyp/README.md)
  * [Träna på Adobe XD](prototyp/traena-pa-adobe-xd.md)
* [Användbarhetstest](anvaendbarhetstest.md)
