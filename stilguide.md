# 3. Stilguide

## Skapa en stilguide för webbplatsen

* Typsnittsval för logga/rubrik/brödtext
  * Välj 3 typsnitt på [Google Fonts](https://fonts.google.com)
* Textens storlek, radlängd och placering
* Skapa en färgpalett utifrån moodboard
  * Använd tex [colormind.io](http://colormind.io) eller [coolors.co](https://coolors.co)
  * Importera moodboard in i en app som plockar ut en färgpalett

## Exempel

* [Vänsterpartiet ](http://stilguide.vansterpartiet.se/page/farger)stilguide för trycksaker och webb
* [Kungliga bibliotekets](https://kungbib.github.io/frontend-guide/section-4.html) stilguide

## Generella designprinciper

### Grundläggande om färgval

{% embed url="https://youtu.be/_2LLXnUdUIc" %}

### Grundläggande om typsnitt

{% embed url="https://youtu.be/sByzHoiYFX0" %}

